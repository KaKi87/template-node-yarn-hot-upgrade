const
    { repository, dependencies } = require('./package.json'),
    fs = require('fs'),
    path = require('path'),
    childProcess = require('child_process'),
    child = dependencies && require('template-node-yarn-hot-upgrade'),
    filePath = path.join(__dirname, 'i'),
    get = () => new Promise(resolve => fs.readFile(filePath, 'utf8', (error, i) => resolve(parseInt(i)))),
    set = i => new Promise(resolve => fs.writeFile(filePath, i.toString(), () => resolve(i))),
    increment = async () => await set(await get() + 1),
    exec = command => new Promise(resolve => childProcess.exec(command, { cwd: __dirname }, (error, res) => resolve(res.trim()))),
    pull = async () => {
        await exec('git init');
        await exec('git config --local user.email nodejs@example.com');
        await exec('git config --local user.name nodejs');
        await exec(`git remote add origin ${repository}`);
        await exec('git fetch');
        await exec('git reset --mixed origin/master');
    },
    push = async () => {
        await exec('git add i');
        await exec('git commit -m ":card_file_box: Increment i"');
        await exec('git push origin master');
    },
    upgrade = () => exec(`yarn add ${repository}`),
    restart = async () => {
        if(fs.existsSync('.stop'))
            await new Promise(resolve => fs.unlink('.stop', resolve));
        else {
            process.on('exit', () => {
                childProcess.spawn(
                    process.argv.shift(),
                    process.argv,
                    {
                        cwd: process.cwd(),
                        detached: true,
                        stdio: 'inherit'
                    }
                );
            });
        }
        process.exit();
    };

module.exports = {
    get,
    increment,
    pull,
    push
};

if(child) (async () => {
    console.log(await child.get());
    await child.pull();
    await child.increment();
    await child.push();
    await upgrade();
    await restart();
})();